## Dártika Meets: Conceptos básicos de Git

### Índice:

#### 1. ¿Qué es Git?

 - Historia (breve)
 - Control de versiones
 - Sistema Centralizado vs Sistema Distrubuido
 - Git != Github
 - Consola vs GUI
 - Ecosistema Git (Stash, Working, Staging, Repository, Remote areas)

#### 2. Comandos básicos de Git

 - `git init`
 - `git clone`
 - `git status`
 - `git add`
 - `git commit`
 - `git log`
 - `git checkout`
 - `git diff`
 - `git stash`
 - .gitignore

#### 3. Respositorios Remotos

 - Alternativas (Github, Gitlab, Bitbucket...)
 - Alias (origin)
 - `git remote`
 - `git fetch`
 - `git pull`
 - `git push`

#### 4. Resolución de conflictos

#### 5. Ramas (branches)

 - `git branch`
 - `git checkout -b`
 - `git merge`
 - `git rebase`

#### 6. Gitflow
